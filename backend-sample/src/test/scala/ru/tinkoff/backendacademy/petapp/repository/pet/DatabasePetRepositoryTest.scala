package ru.tinkoff.backendacademy.petapp.repository.pet

import com.typesafe.config.ConfigFactory
import eu.rekawek.toxiproxy.model.ToxicDirection
import io.getquill.JdbcContextConfig
import io.getquill.context.ZioJdbc
import org.testcontainers.containers.PostgreSQLContainer.POSTGRESQL_PORT
import org.testcontainers.containers.{Network, PostgreSQLContainer, ToxiproxyContainer}
import ru.tinkoff.backendacademy.petapp.db.MigrateDB
import zio.clock.Clock
import zio.test.Assertion.equalTo
import zio.test.{DefaultRunnableSpec, assertM}
import zio.{Has, ZIO, ZLayer, ZManaged}

import scala.jdk.CollectionConverters.MapHasAsJava

object DatabasePetRepositoryTest extends DefaultRunnableSpec {

  override def spec =
    suite("Pet service database interactions")(
      testM("Database pet repository works with poor network") {
        (for {
          network   <- ZManaged.fromAutoCloseable(ZIO.effect(Network.newNetwork)).toLayer
          pc        <- postgresLayer(network.get)
          toxiproxy <- toxiproxyLayer(network.get)
          proxy                 = toxiproxy.get.getProxy(pc.get, POSTGRESQL_PORT)
          ipAddressViaToxiproxy = proxy.getContainerIpAddress
          portViaToxiproxy      = proxy.getProxyPort
          databaseConfigToxiProxy = JdbcContextConfig(
            ConfigFactory.parseMap(
              Map(
                "jdbcUrl" -> s"jdbc:postgresql://$ipAddressViaToxiproxy:$portViaToxiproxy/${pc.get.getDatabaseName}",
                "username"          -> pc.get.getUsername,
                "password"          -> pc.get.getPassword,
                "driverClassName"   -> pc.get.getDriverClassName,
                "connectionTimeout" -> 250,
                "validationTimeout" -> 300
              ).asJava
            )
          )
          dataSource     <- ZioJdbc.DataSourceLayer.fromJdbcConfig(databaseConfigToxiProxy)
          _              <- MigrateDB.run("migration.sql", dataSource.get).toLayer
          consoleLogging <- zio.logging.Logging.console()
          clock          <- Clock.live
          databasePetRepository =
            new RetryingPetRepository(
              clock,
              new LoggingPetRepository(
                consoleLogging,
                new DatabasePetRepository(dataSource)
              )
            )
          _ <- ZIO.effect {
            proxy
              .toxics()
              .latency("latency", ToxicDirection.DOWNSTREAM, 400)
              .setToxicity(0.9.toFloat)
          }.toLayer
        } yield databasePetRepository).build.use { repo =>
          assertM(repo.findPetName(7))(equalTo("Mr. name7"))
        }
      }
    )

  private val networkAlias = "toxiproxy"

  private def postgresLayer(network: Network): ZLayer[Any, Throwable, Has[PostgreSQLContainer[_]]] =
    ZManaged
      .fromAutoCloseable(ZIO.effect {
        val container = (new PostgreSQLContainer("postgres:alpine"): PostgreSQLContainer[_])
          .withNetwork(network)
        container.start()
        container
      })
      .toLayer

  private def toxiproxyLayer(network: Network): ZLayer[Any, Throwable, Has[ToxiproxyContainer]] = {
    ZManaged
      .fromAutoCloseable(ZIO.effect {
        import org.testcontainers.containers.ToxiproxyContainer
        import org.testcontainers.utility.DockerImageName

        val toxiproxyImage = DockerImageName
          .parse("ghcr.io/shopify/toxiproxy:2.4.0")
          .asCompatibleSubstituteFor("shopify/toxiproxy")
        val container = new ToxiproxyContainer(toxiproxyImage)
          .withNetwork(network)
          .withNetworkAliases(networkAlias)
        container.start()
        container
      })
      .toLayer
  }

}
