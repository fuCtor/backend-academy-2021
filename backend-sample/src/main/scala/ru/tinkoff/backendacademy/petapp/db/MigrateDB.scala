package ru.tinkoff.backendacademy.petapp.db

import com.typesafe.config.ConfigFactory
import io.getquill.JdbcContextConfig
import io.getquill.context.ZioJdbc
import liquibase.database.DatabaseFactory
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.ClassLoaderResourceAccessor
import liquibase.{Contexts, Liquibase}
import zio._
import zio.blocking.{Blocking, effectBlocking}

import javax.sql.DataSource

object MigrateDB extends zio.App {

  override def run(args: List[String]): URIO[zio.ZEnv, ExitCode] = ZIO.effect {
    val databaseConfig =
      JdbcContextConfig(ConfigFactory.defaultApplication().resolve().getConfig("database"))
    ZioJdbc.DataSourceLayer.fromJdbcConfig(databaseConfig).build.use { ds =>
      run(args.head, ds.get)
    }
  }.exitCode

  def run(
      changeLogFile: String,
      dataSource: DataSource
  ): ZIO[Blocking, Throwable, Unit] = {
    val connection = dataSource.getConnection
    effectBlocking {
      val database = DatabaseFactory.getInstance
        .findCorrectDatabaseImplementation(new JdbcConnection(connection))
      val classLoader      = classOf[MigrateDB.type].getClassLoader
      val resourceAccessor = new ClassLoaderResourceAccessor(classLoader)
      val liquibase        = new Liquibase(changeLogFile, resourceAccessor, database)
      liquibase.update(new Contexts())
    }
  }
}
