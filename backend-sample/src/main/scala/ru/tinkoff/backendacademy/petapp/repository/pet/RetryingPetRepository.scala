package ru.tinkoff.backendacademy.petapp.repository.pet

import zio.clock.Clock
import zio.{Schedule, ZIO}

import java.time.Duration

class RetryingPetRepository(clock: Clock, delegate: PetRepository) extends PetRepository {
  override def findPetName(petId: Long): ZIO[Any, String, String] =
    delegate.findPetName(petId)
      .retry(Schedule.fibonacci(Duration.ofMillis(10)))
      .provide(clock)
}
