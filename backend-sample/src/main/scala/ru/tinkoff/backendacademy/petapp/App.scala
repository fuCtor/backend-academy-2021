package ru.tinkoff.backendacademy.petapp

import cats.effect.std.Dispatcher
import com.typesafe.config.ConfigFactory
import io.getquill.context.ZioJdbc
import io.getquill.{JdbcContextConfig, PostgresZioJdbcContext, SnakeCase}
import ru.tinkoff.backendacademy.petapp.http.{HttpServer, OwnerEndpoints, PetEndpoints}
import ru.tinkoff.backendacademy.petapp.repository.owner.DatabaseOwnerRepository
import ru.tinkoff.backendacademy.petapp.repository.pet.{DatabasePetRepository, LoggingPetRepository}
import zio.clock.Clock
import zio.console.Console
import zio.{ExitCode, Has, Task, URIO, ZEnv, ZIO, ZLayer}

import javax.sql.DataSource
import scala.concurrent.Future

/*

database problems
* database connection config
** credentials issue
** database URL
* network
** latency
* database cannot handle huge load
* slow database
* database outage
* capacity problems
** disk
** connections
** memory
** cpu
* data corruption

http server problems
* network
** open sockets count (FDs)
** bandwidth
** latency

application runtime
* out of memory
* JVM crushes (segfault, ...)
* false CPU instructions

infrastructure
* power outage
* segv

*/


@scala.annotation.nowarn("cat=unused")
object App extends zio.App {

  val ctx = new PostgresZioJdbcContext[SnakeCase](SnakeCase)

  override def run(args: List[String]): URIO[ZEnv, ExitCode] = {
    val databaseConfig = JdbcContextConfig(ConfigFactory.defaultApplication().resolve().getConfig("database"))
    run(databaseConfig)
  }

  def run(databaseConfig: JdbcContextConfig): ZIO[Console with Clock, Nothing, ExitCode] = {
    val dataSource: ZLayer[Any, Throwable, Has[DataSource]] =
      ZioJdbc.DataSourceLayer.fromJdbcConfig(databaseConfig)

    ZIO
      .runtime[Any]
      .flatMap { implicit runtime =>
        (for {
          ds <- dataSource
          consoleLogging <- zio.logging.Logging.console()
          databasePetRepository = new LoggingPetRepository(
            consoleLogging,
            new DatabasePetRepository(ds)
          )
          databaseOwnerRepository = new DatabaseOwnerRepository(ds)
          endpoints = List(PetEndpoints(databasePetRepository)) ++
            OwnerEndpoints(databaseOwnerRepository)
          server = new HttpServer(endpoints)
        } yield server).build.orDie.use { server =>
          server.server.exitCode
        }
      }
  }

  private implicit def dispatcher(implicit runtime: zio.Runtime[Any]): Dispatcher[Task] = {
    new Dispatcher[Task] {
      override def unsafeToFutureCancelable[A](fa: Task[A]): (Future[A], () => Future[Unit]) = {
        val value = unsafeRunToFuture(fa)
        (value.future, () => value.cancel().map(_ => ())(runtime.platform.executor.asEC))
      }
    }
  }

}
